<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::view('test','tutor.QR');

Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware'=>'admin'],function (){
    Route::get('/','AdminPanel\AdminController@index')->name('index');
    Route::resource('student','AdminPanel\studentController');
    Route::resource('tutor','AdminPanel\tutorController');
    Route::resource('admin','AdminPanel\adminUserController');
    Route::resource('lab','AdminPanel\labController');
    Route::post('/password/{id}','AdminPanel\studentController@updatePassword')->name('updatePassword');
    Route::post('/admin/password/{id}','AdminPanel\adminUserController@updatePassword')->name('adminUpdatePassword');
    Route::get('/attend/{id}','AdminPanel\sessionController@attends')->name('attendBySession');
    Route::get('student/attend/{id}','AdminPanel\studentController@attends')->name('studentAttend');
    Route::get('lab/session/{id}','AdminPanel\labController@session')->name('sessionLab');
    Route::get('tutor/session/{id}','AdminPanel\tutorController@session')->name('tutorSession');
    Route::resource('session','AdminPanel\sessionController');
    Route::get('student/MAC/{id}','AdminPanel\studentController@resethMAC')->name('studentMAC');
    Route::get('tutor/MAC/{id}','AdminPanel\tutorController@resetMAC')->name('tutorMAC');

    });


    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');
 /*   Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AdminAuth\RegisterController@register');*/
    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['middleware'=>'auth:web'],function (){
Route::get('Qr','Qr@index');
Route::get('Qr/session/{id}','Qr@Qr')->name('QR');
Route::get('Qr/refresh/{id}','Qr@refresh')->name('QrRefresh');
});
Route::view('notTutor','tutor.ForTutorOnly');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');
