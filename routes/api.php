<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['middleware'=>'auth:api'],function(){
   Route::resource('location','API\LocationsController');
   Route::resource('session','API\SessionController');
   Route::resource('attend','API\AttendController');
   Route::post('attend/show','API\AttendController@showById');
   Route::post('attend/delete','API\AttendController@attendTutor');
   Route::post('attend/isAttend','API\AttendController@isAttend');
   Route::post('attend/byTutor','API\ProfileController@allTutorAttends');
   Route::post('attend/byDate','API\ProfileController@byDate');
   Route::post('attend/allAttends','API\ProfileController@allAttends');
   Route::post('MAC','API\ProfileController@SetMac');
   Route::post('images','API\photoController@upload');
   Route::post('session/allSessions','API\ProfileController@allSessions');
   Route::post('session/byDate','API\SessionController@SessionByDate');
   Route::post('session/tutorByDate','API\SessionController@SessionTutorByDate');
   Route::post('password','API\ProfileController@updatePassword');
});
   

Route::post('/login','API\LoginController@Login');