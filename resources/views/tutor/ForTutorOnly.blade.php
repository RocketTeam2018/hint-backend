@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Not authenticated</div>
                    <div class="card-body">
                        <h1>Sorry this feature for tutors Only</h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
