<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Hint') }}</title>

    <!-- Scripts -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
{{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
{!! Html::script('js/jquery-3.3.1.min.js') !!}
<!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                Hint
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                    <!-- Authentication Links -->

                            <div class="" aria-labelledby="">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
            </div>
        </div>
    </nav>

    <main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Welcome Tutor {{auth()->user()->name}}</div>
                    <div class="card-body">
                        <div class="center-block">
                            <table id="basic_table" class="table_top_content table  table-striped">
                                <thead>
                                <tr>
                                    <th>Course Name</th>
                                    <th>lab</th>
                                    <th>tutor</th>
                                    <th>time</th>
                                    <th>Action</th>
                                </tr> <!-- /tr -->
                                </thead> <!-- /thead -->
                                <tbody>
                                @foreach($sessions as $key=> $session)
                                    <tr>
                                        <td>{{$session->courseName}}</td>
                                        <td>{{$session->lab->name}}</td>
                                        <td>{{$session->doctor->name}}</td>
                                        <td>{{$session->time}}</td>
                                        <td>
                                            <a href="{{route('QR',['id'=>$session->id])}}">QR</a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody> <!-- /tbody -->
                                <tfoot>
                                <tr>
                                    <th>Course Name</th>
                                    <th>lab</th>
                                    <th>tutor</th>
                                    <th>time</th>
                                    <th>Action</th>
                                </tr> <!-- /tr -->
                                </tfoot> <!-- /tfoot -->
                            </table>
                            {{--   @if($session==null)
                                   <h1>There is No Sessions !</h1>
                                   @else
                               <h1>The Qr Code available for session <a href="{{route('QR')}}">{{$session->courseName}}</a> </h1>
                                   @endif--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </main>
</div>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script !src="">
        $(document).ready(function () {
            "use strict";
            var table = $('#basic_table');
            table.dataTable({
                "paging": true,
                "ordering": true,
                "info": false,
                'search':false,
                "order": [[ 3, "desc" ]]
            });
            /*table.DataTable({
                "dom": '<<"" <"dataTable_top left col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-0"i<"clear">> <"dataTable_top right col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pr-0"f<"clear">> >rt<"bottom"p<"clear">>',
                "oLanguage": {"sSearch": ""}
            });*/
        });
    </script>
</body>
</html>