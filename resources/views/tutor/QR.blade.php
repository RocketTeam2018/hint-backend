@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Qr for session {{$session->courseName}}</div>
                    <div class="card-body">
                        <div class="center-block" id="qr"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {!! Html::script('js/jquery-3.3.1.min.js') !!}
    {!! Html::script('js/qrcode.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            var qrcode = new QRCode(document.getElementById('qr'), {
                text: "{{$session->token}}",
                width: 600,
                height: 600,
                colorDark: "#000000",
                colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });

            function QrRefresh() {
                $.ajax({
                    type: "GET",
                    url: "{{route('QrRefresh',['id'=>$session->id])}}",
                    success: function (data) {
                        console.log(data.token);
                        qrcode.clear();
                        qrcode.makeCode(data.token);
                    }
                });
            }

            setInterval(function () {
                QrRefresh()
            }, 10000);
        });
    </script>
@endsection