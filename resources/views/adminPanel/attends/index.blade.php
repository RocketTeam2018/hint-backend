@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>attends</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="#">Home</a>
                <span class="breadcrumb-item active">attends</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                </div> <!-- /card-heading -->
                <div class="card-body">

                    <table id="basic_table" class="table_top_content table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Student</th>
                            <th>Course</th>
                            <th>Created At</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        @foreach($attends as $key=> $attend)
                        <tr>
                            <td>{{$attend->student->name}}</td>
                            <td>{{$attend->session->courseName}}</td>
                            <td>{{$attend->created_at}}</td>
                        </tr>
                        @endforeach

                        </tbody> <!-- /tbody -->
                        <tfoot>
                        <tr>
                            <th>Student</th>
                            <th>Course</th>
                            <th>Created At</th>
                        </tr> <!-- /tr -->
                        </tfoot> <!-- /tfoot -->
                    </table> <!-- /table-responsive -->
                </div> <!-- /card-body -->
            </div> <!-- /card -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            "use strict";
            var table = $('.table_top_content');
            table.DataTable({
                "dom": '<<"" <"dataTable_top left col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-0"i<"clear">> <"dataTable_top right col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pr-0"f<"clear">> >rt<"bottom"p<"clear">>',
                "oLanguage": { "sSearch": "" }
            });
        });
    </script>
    @endsection
