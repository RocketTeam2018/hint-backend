<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Zwolek">
    <link rel="icon" href="../img/favicon.ico">
    <title>Vega Admin</title>
    <!-- Plugins Styles -->
    {!! Html::style('css/plugins.min.css') !!}
    <!-- Core Styles -->
    {!! Html::style('css/app.min.css') !!}
</head>
<body>

<!-- ////////// Preloader //////////-->
<div id="preloader"></div>

<div class="container">

    <div class="row">

        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">

            <div class="authentication">

                <div class="authentication-wrapper">

                    <div class="authentication-header">
                        <h4>Sign In</h4>
                    </div> <!-- /authentication-header -->

                    <div class="authentication-content">


                            {!! Form::open(['url'=>url('/admin/login'),'method'=>'post','role'=>'form','data-toggle'=>"validator"]) !!}

                            <div class="form-group">
                                <label for="e_mail" class="form-label">Email</label>
                                <div class="input-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                    <input type="email" class="form-control" name="email" id="e_mail" placeholder="Enter email" data-error="Bruh, that email address is invalid" required>
                                </div> <!-- /input-group -->
                                <div class="help-block with-errors">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div> <!-- /form-group -->

                            <div class="form-group">
                                <label for="password" class="form-label">Password</label>
                                <div class="input-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
                                    <input class="form-control" name="password" type="password" placeholder="Password" id="password"
                                           data-error="Bruh, that password is invalid" required>
                                </div> <!-- /input-group -->
                                <div class="help-block with-errors">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div> <!-- /form-group -->

                            <div class="form-group form-check">
                                <div class="checkbox checkbox-primary">
                                    <input id="check_remember" class="check" name="remember" type="checkbox"/>
                                    <label for="check_remember">Remember Me</label>
                                </div> <!-- /checkbox -->
                            </div> <!-- /form-group form-check -->

                            <div class="form-footer">
                                <a href="{{ url('/admin/password/reset') }}" class="link">Forgot Password ?</a>
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div> <!-- /form-footer -->

                       {!! Form::close() !!}<!-- /form -->

                    </div> <!-- /authentication-content -->

                </div> <!-- /authentication-wrapper -->

            </div> <!-- /authentication -->

        </div> <!-- /col -->

    </div> <!-- /row -->

</div> <!-- /container -->

<!-- Core Plugins (necessary for Vega plugins)  -->
{!! Html::script('js/app.min.js') !!}
<!-- Custom Scripts -->
{!! Html::script('assets/scripts/custom.js') !!}
</body>
</html>

