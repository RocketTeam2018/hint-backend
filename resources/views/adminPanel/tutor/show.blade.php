@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>tutors</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="{{route('index')}}">Home</a>
                <a class="breadcrumb-item" href="{{route('tutor.index')}}">tutors</a>
                <span class="breadcrumb-item active">tutor {{$tutor->name}}</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                    <h5>tutor {{$tutor->name}}</h5>
                </div> <!-- /card-heading -->
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>#</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{{$tutor->name}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th>Email</th>
                            <td>{{$tutor->email}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th >Phone</th>
                            <td>{{$tutor->phone}}</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>{!! Html::image($tutor->image,'',['class'=>'img-responsive','width'=>'250px','height'=>'250px']) !!}</td>
                        </tr>
                        <tr>
                            <td>Sessions</td>
                            <td><a href="{{route('tutorSession',['id'=>$tutor->uuid])}}"><i class="fa fa-book"></i></a></td>
                        </tr>
                        <tr>
                            <td>Reset MAC</td>
                            <td><a href="{{route('tutorMAC',['id'=>$tutor->uuid])}}">Reset</a></td>
                        </tr>
                        <!-- /tr -->
                        </tbody> <!-- /tbody -->
                    </table> <!-- /table -->
                </div> <!-- /card-body -->
            </div>
        </div>
    </div>
@endsection

