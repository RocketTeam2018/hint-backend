@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>sessions</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="#">Home</a>
                <span class="breadcrumb-item active">sessions</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                </div> <!-- /card-heading -->
                <div class="card-body">

                    <table id="basic_table" class="table_top_content table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>Course Name</th>
                            <th>lab</th>
                            <th>tutor</th>
                            <th>time</th>
                            <th>Action</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        @foreach($sessions as $key=> $session)
                        <tr>
                            <td>{{$session->courseName}}</td>
                            <td>{{$session->lab->name}}</td>
                            <td>{{$session->doctor->name}}</td>
                            <td>{{$session->time}}</td>
                            <td>
                                <a href="{{route('session.show',['id'=>$session->id])}}"><i class="fa fa-eye"></i></a>
                                &nbsp;
                                <a onclick="$('#{{$key}}delete').submit()"><i class="fa fa-trash"></i> </a>
                                {!! Form::open(['url'=>route('session.destroy',['id'=>$session->id]),'method'=>'delete','id'=>$key.'delete']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach

                        </tbody> <!-- /tbody -->
                        <tfoot>
                        <tr>
                            <th>Course Name</th>
                            <th>lab</th>
                            <th>tutor</th>
                            <th>time</th>
                            <th>Action</th>
                        </tr> <!-- /tr -->
                        </tfoot> <!-- /tfoot -->
                    </table> <!-- /table-responsive -->
                </div> <!-- /card-body -->
            </div> <!-- /card -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            "use strict";
            var table = $('.table_top_content');
            table.DataTable({
                "dom": '<<"" <"dataTable_top left col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-0"i<"clear">> <"dataTable_top right col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pr-0"f<"clear">> >rt<"bottom"p<"clear">>',
                "oLanguage": { "sSearch": "" }
            });
        });
    </script>
    @endsection
