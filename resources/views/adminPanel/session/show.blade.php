@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>sessions</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="{{route('index')}}">Home</a>
                <a class="breadcrumb-item" href="{{route('session.index')}}">sessions</a>
                <span class="breadcrumb-item active">session {{$session->name}}</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                    <h5>session {{$session->name}}</h5>
                </div> <!-- /card-heading -->
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>#</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        <tr>
                            <th>Course Name</th>
                            <td>{{$session->courseName}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th>lab</th>
                            <td>{{$session->lab->name}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th >tutor</th>
                            <td>{{$session->doctor->name}}</td>
                        </tr>
                        <tr>
                            <td>time</td>
                            <td>{{$session->time}}</td>
                        </tr>
                        <tr>
                            <td>Attends</td>
                            <td><a href="{{route('attendBySession',['id'=>$session->id])}}"><i class="fa fa-book"></i></a></td>
                        </tr>
                        <!-- /tr -->
                        </tbody> <!-- /tbody -->
                    </table> <!-- /table -->
                </div> <!-- /card-body -->
            </div>
        </div>
    </div>
@endsection

