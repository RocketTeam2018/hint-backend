<div class="card">
    <div class="card-heading">
        <h5>student</h5>
    </div> <!-- /card-heading -->
    <div class="card-body">
        {!! Form::open(['url'=>isset($student)?route('student.update',['id'=>$student->uuid]):route('student.store'),'method'=> isset($student)?'put':'post','files'=>true]) !!}


        @if(!isset($student))
        <div class="form-group {{ $errors->has('uuid') ? ' has-error' : '' }}">
            <label for="example-name-input" class="form-label">ID</label>
            <input class="form-control" type="text" placeholder="ID" name="uuid"
                   value="{{isset($student)?$student->uuid:old("uuid")}}"
                   id="example-name-input">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    uuid
                @endslot
            @endcomponent
        </div>
        @endif
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="example-name-input" class="form-label">Name</label>
            <input class="form-control" type="text" placeholder="Name" name="name"
                   value="{{isset($student)?$student->name:old("name")}}"
                   id="example-name-input">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    name
                @endslot
            @endcomponent
        </div> <!-- /form-group -->


        @if(!isset($student))
        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail2">Email address</label>
            <input type="email" class="form-control" id="" name="email"
                   value="{{isset($student)?$student->email:old("email")}}"
                   placeholder="Enter email">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    email
                @endslot
            @endcomponent
        </div> <!-- /form-group -->
        @endif
        @if(!isset($student))
            <div class="row {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputPassword3" class="form-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword3"
                               name="password" placeholder="Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputConfrimPassword3" class="form-label">Confrim Password</label>
                        <input type="password" class="form-control" id="inputConfrimPassword3"
                               name="password_confirmation" placeholder="Confrim Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                @component('adminPanel.layouts.errors')
                    @slot('slot')
                        password
                    @endslot
                @endcomponent
            </div> <!-- /row -->
        @endif
        <div class="form-group">
            <label for="example-text-input" class="form-label">Phone</label>
            <input class="form-control" type="text" name="phone" placeholder="phone"
                   value="{{isset($student)?$student->phone:old("phone")}}" id="example-text-input">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    phone
                @endslot
            @endcomponent
        </div> <!-- /form-group -->

        <div class="form-group">

            @if(isset($student))
                {!! Html::image($student->image,'',['class'=>'img-responsive','width'=>'300px','height'=>'300px']) !!}
            @endif
        </div>
        <div class="form-group  {{ $errors->has('photo') ? ' has-error' : '' }}">
            <label class="form-label"> Profile Picture</label>
            <div class="input-group file-upload">
                <label class="custom-file pull-left">
                    <input type="file" id="file" name="photo" class="custom-file-input">
                    <span class="custom-file-control"></span>
                </label> <!-- /custom-file -->
                @component('adminPanel.layouts.errors')
                    @slot('slot')
                        photo
                    @endslot
                @endcomponent
            </div> <!-- /form-group -->
        </div>


        <button type="submit" class="btn btn-primary pull-right">Submit</button>
    {!! Form::close() !!} <!-- /form -->
    </div>
    <div class="card-body">

    </div>
</div>


@if(isset($student))
    <br>
    <div class="card">
        <div class="card-heading">
            <h5>Update student Password <i class="fa fa-pencil"></i></h5>
        </div> <!-- /card-heading -->
        <div class="card-body">
            {!! Form::open(['url'=>route('updatePassword',['id'=>$student->uuid]),'method'=> 'post']) !!}
            <div class="row {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputPassword3" class="form-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword3"
                               name="password" placeholder="Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputConfrimPassword3" class="form-label">Confrim Password</label>
                        <input type="password" class="form-control" id="inputConfrimPassword3"
                               name="password_confirmation" placeholder="Confrim Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                @component('adminPanel.layouts.errors')
                    @slot('slot')
                        password
                    @endslot
                @endcomponent
            </div> <!-- /row -->
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>
    @endif