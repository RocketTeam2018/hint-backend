@extends('adminPanel.layouts.main')
@section('content')
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <a href="{{route('student.index')}}">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="stats-widget stats-style-1 bg-primary">
                            <div class="stats-header">
                                <h5 class="stats-title">Students <i class="fa fa-users"></i></h5>
                                <p class="stats-text">{{\App\User::where('type',0)->count()}}</p>
                            </div> <!-- /stats-header -->
                            {{--   <div class="stats-body">
                                   <div class="stats-container">
                                       <div id="stats_style_1_1" class="stats-chart"></div>
                                   </div> <!-- /stats-container -->
                               </div> <!-- /stats-body -->--}}
                        </div> <!-- /stats-widget stats-2 -->
                    </div> <!-- /card-body -->
                </a>
            </div> <!-- /card -->
        </div>
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <a href="{{route('tutor.index')}}">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="stats-widget stats-style-1 bg-primary">
                            <div class="stats-header">
                                <h5 class="stats-title">Tutors <i class="fa fa-user"></i></h5>
                                <p class="stats-text">{{\App\User::where('type',1)->count()}}</p>
                            </div> <!-- /stats-header -->
                            {{--   <div class="stats-body">
                                   <div class="stats-container">
                                       <div id="stats_style_1_1" class="stats-chart"></div>
                                   </div> <!-- /stats-container -->
                               </div> <!-- /stats-body -->--}}
                        </div> <!-- /stats-widget stats-2 -->
                    </div> <!-- /card-body -->
                </a>
            </div> <!-- /card -->
        </div>
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <a href="{{route('admin.index')}}">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="stats-widget stats-style-1 bg-primary">
                            <div class="stats-header">
                                <h5 class="stats-title">Admin <i class="fa fa-lock"></i></h5>
                                <p class="stats-text">{{\App\Admin::all()->count()}}</p>
                            </div> <!-- /stats-header -->
                            {{--   <div class="stats-body">
                                   <div class="stats-container">
                                       <div id="stats_style_1_1" class="stats-chart"></div>
                                   </div> <!-- /stats-container -->
                               </div> <!-- /stats-body -->--}}
                        </div> <!-- /stats-widget stats-2 -->
                    </div> <!-- /card-body -->
                </a>
            </div> <!-- /card -->
        </div>
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <a href="{{route('admin.index')}}">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="stats-widget stats-style-1 bg-primary">
                            <div class="stats-header">
                                <h5 class="stats-title">Lab <i class="fa fa-location-arrow"></i></h5>
                                <p class="stats-text">{{\App\Location::all()->count()}}</p>
                            </div> <!-- /stats-header -->
                            {{--   <div class="stats-body">
                                   <div class="stats-container">
                                       <div id="stats_style_1_1" class="stats-chart"></div>
                                   </div> <!-- /stats-container -->
                               </div> <!-- /stats-body -->--}}
                        </div> <!-- /stats-widget stats-2 -->
                    </div> <!-- /card-body -->
                </a>
            </div> <!-- /card -->
        </div>
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <a class="anchor" href="{{route('session.index')}}">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="stats-widget stats-style-1 bg-primary">
                            <div class="stats-header">
                                <h5 class="stats-title">Session <i class="fa fa-book"></i></h5>
                                <p class="stats-text">{{\App\Session::all()->count()}}</p>
                            </div> <!-- /stats-header -->
                            {{--   <div class="stats-body">
                                   <div class="stats-container">
                                       <div id="stats_style_1_1" class="stats-chart"></div>
                                   </div> <!-- /stats-container -->
                               </div> <!-- /stats-body -->--}}
                        </div> <!-- /stats-widget stats-2 -->
                    </div> <!-- /card-body -->
                </a>
            </div> <!-- /card -->
        </div>
        <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-3 mt-4">
            <div class="card">
                <div class="card-body pl-0 pr-0 pb-0">
                    <div class="stats-widget stats-style-1 bg-primary">
                        <div class="stats-header">
                            <h5 class="stats-title">Attends <i class="fa fa-leanpub"></i></h5>
                            <p class="stats-text">{{\App\Attend::all()->count()}}</p>
                        </div> <!-- /stats-header -->
                        {{--   <div class="stats-body">
                               <div class="stats-container">
                                   <div id="stats_style_1_1" class="stats-chart"></div>
                               </div> <!-- /stats-container -->
                           </div> <!-- /stats-body -->--}}
                    </div> <!-- /stats-widget stats-2 -->
                </div> <!-- /card-body -->
            </div> <!-- /card -->
        </div>
    </div>
@endsection
