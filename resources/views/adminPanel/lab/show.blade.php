@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>labs</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="{{route('index')}}">Home</a>
                <a class="breadcrumb-item" href="{{route('lab.index')}}">labs</a>
                <span class="breadcrumb-item active">lab {{$lab->name}}</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                    <h5>lab {{$lab->name}}</h5>
                </div> <!-- /card-heading -->
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>#</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{{$lab->name}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th>location</th>
                            <td>{{$lab->location}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th >MAC</th>
                            <td>{{$lab->MAC}}</td>
                        </tr>
                        <tr>
                            <td>Sessions</td>
                            <td><a href="{{route('sessionLab',['id'=>$lab->id])}}"><i class="fa fa-leanpub"></i></a></td>
                        </tr>
                        <!-- /tr -->
                        </tbody> <!-- /tbody -->
                    </table> <!-- /table -->
                </div> <!-- /card-body -->
            </div>
        </div>
    </div>
@endsection

