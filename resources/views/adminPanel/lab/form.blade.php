<div class="card">
    <div class="card-heading">
        <h5>lab</h5>
    </div> <!-- /card-heading -->
    <div class="card-body">
        {!! Form::open(['url'=>isset($lab)?route('lab.update',['id'=>$lab->id]):route('lab.store'),'method'=> isset($lab)?'put':'post','files'=>true]) !!}


        <div class="form-group {{ $errors->has('name') ? ' has-danger' : '' }}">
            <label for="example-name-input" class="form-label">Name</label>
            <input class="form-control" type="text" placeholder="Name" name="name"
                   value="{{isset($lab)?$lab->name:old("name")}}"
                   id="example-name-input">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    name
                @endslot
            @endcomponent
        </div> <!-- /form-group -->



        <div class="form-group  {{ $errors->has('location') ? ' has-danger' : '' }}">
            <label for="exampleInputEmail2">location</label>
            <input type="text" class="form-control" id="" name="location"
                   value="{{isset($lab)?$lab->location:old("location")}}"
                   placeholder="Enter location">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    location
                @endslot
            @endcomponent
        </div> <!-- /form-group -->

        <div class="form-group  {{ $errors->has('MAC') ? ' has-danger' : '' }}">
            <label for="exampleInputEmail2">MAC Address</label>
            <input type="text" class="form-control" id="" name="MAC"
                   value="{{isset($lab)?$lab->MAC:old("MAC")}}"
                   placeholder="Enter location">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    MAC
                @endslot
            @endcomponent
        </div> <!-- /form-group -->


        <button type="submit" class="btn btn-primary pull-right">Submit</button>
    {!! Form::close() !!} <!-- /form -->
    </div>
    <div class="card-body">

    </div>
</div>


