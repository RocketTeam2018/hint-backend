<div class="card">
    <div class="card-heading">
        <h5>admin</h5>
    </div> <!-- /card-heading -->
    <div class="card-body">
        {!! Form::open(['url'=>isset($admin)?route('admin.update',['id'=>$admin->id]):route('admin.store'),'method'=> isset($admin)?'put':'post','files'=>true]) !!}


        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="example-name-input" class="form-label">Name</label>
            <input class="form-control" type="text" placeholder="Name" name="name"
                   value="{{isset($admin)?$admin->name:old("name")}}"
                   id="example-name-input">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    name
                @endslot
            @endcomponent
        </div> <!-- /form-group -->


        @if(!isset($admin))
        <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="exampleInputEmail2">Email address</label>
            <input type="email" class="form-control" id="" name="email"
                   value="{{isset($admin)?$admin->email:old("email")}}"
                   placeholder="Enter email">
            @component('adminPanel.layouts.errors')
                @slot('slot')
                    email
                @endslot
            @endcomponent
        </div> <!-- /form-group -->
        @endif
        @if(!isset($admin))
            <div class="row {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputPassword3" class="form-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword3"
                               name="password" placeholder="Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputConfrimPassword3" class="form-label">Confrim Password</label>
                        <input type="password" class="form-control" id="inputConfrimPassword3"
                               name="password_confirmation" placeholder="Confrim Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                @component('adminPanel.layouts.errors')
                    @slot('slot')
                        password
                    @endslot
                @endcomponent
            </div> <!-- /row -->
        @endif

        <button type="submit" class="btn btn-primary pull-right">Submit</button>
    {!! Form::close() !!} <!-- /form -->
    </div>
    <div class="card-body">

    </div>
</div>


@if(isset($admin))
    <br>
    <div class="card">
        <div class="card-heading">
            <h5>Update admin Password <i class="fa fa-pencil"></i></h5>
        </div> <!-- /card-heading -->
        <div class="card-body">
            {!! Form::open(['url'=>route('adminUpdatePassword',['id'=>$admin->id]),'method'=> 'post']) !!}
            <div class="row {{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputPassword3" class="form-label">Password</label>
                        <input type="password" class="form-control" id="inputPassword3"
                               name="password" placeholder="Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                <div class="col-xs col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label for="inputConfrimPassword3" class="form-label">Confrim Password</label>
                        <input type="password" class="form-control" id="inputConfrimPassword3"
                               name="password_confirmation" placeholder="Confrim Password">
                    </div> <!-- /form-group -->
                </div> <!-- /col -->
                @component('adminPanel.layouts.errors')
                    @slot('slot')
                        password
                    @endslot
                @endcomponent
            </div> <!-- /row -->
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
            {!! Form::close() !!}
        </div>
    </div>
    @endif