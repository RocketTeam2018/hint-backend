@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>students</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="{{route('index')}}">Home</a>
                <a class="breadcrumb-item" href="{{route('student.index')}}">students</a>
                <span class="breadcrumb-item active">student {{$student->name}}</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                    <h5>Student {{$student->name}}</h5>
                </div> <!-- /card-heading -->
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>#</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{{$student->name}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th>Email</th>
                            <td>{{$student->email}}</td>

                        </tr> <!-- /tr -->
                        <tr>
                            <th >Phone</th>
                            <td>{{$student->phone}}</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>{!! Html::image($student->image,'',['class'=>'img-responsive','width'=>'250px','height'=>'250px']) !!}</td>
                        </tr>
                        <!-- /tr -->
                        </tbody> <!-- /tbody -->
                    </table> <!-- /table -->
                </div> <!-- /card-body -->
            </div>
        </div>
    </div>
@endsection

