@extends('adminPanel.layouts.main')
@section('content')
    <!-- ////////// Page Title & Breadcrumbs //////////-->
    <div class="row">
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h4>admins</h4>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="#">Home</a>
                <span class="breadcrumb-item active">admins</span>
            </nav> <!-- /breadcrumb -->
        </div> <!-- /col -->
    </div> <!-- /row -->

    <div class="row">

        <!-- ////////// Basic DataTable //////////-->
        <div class="col-xs col-sm-12 col-md-12 col-lg-12 col-xl-12 mt-4">
            <div class="card">
                <div class="card-heading">
                    <h5><a href="{{route('admin.create')}}">Add new admin <i class="fa fa-plus"></i></a></h5>
                </div> <!-- /card-heading -->
                <div class="card-body">

                    <table id="basic_table" class="table_top_content table table-responsive table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr> <!-- /tr -->
                        </thead> <!-- /thead -->
                        <tbody>
                        @foreach($admins as $key=> $admin)
                        <tr>
                            <td>{{$admin->id}}</td>
                            <td>{{$admin->name}}</td>
                            <td>{{$admin->email}}</td>
                            <td>{{$admin->created_at}}</td>
                            <td>
                                <a href="{{route('admin.edit',['id'=>$admin->id])}}"><i class="fa fa-pencil"></i></a>
                                &nbsp;
                                <a onclick="$('#{{$key}}delete').submit()"><i class="fa fa-trash"></i> </a>
                                {!! Form::open(['url'=>route('admin.destroy',['id'=>$admin->id]),'method'=>'delete','id'=>$key.'delete']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach

                        </tbody> <!-- /tbody -->
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr> <!-- /tr -->
                        </tfoot> <!-- /tfoot -->
                    </table> <!-- /table-responsive -->
                </div> <!-- /card-body -->
            </div> <!-- /card -->
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            "use strict";
            var table = $('.table_top_content');
            table.DataTable({
                "dom": '<<"" <"dataTable_top left col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-0"i<"clear">> <"dataTable_top right col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pr-0"f<"clear">> >rt<"bottom"p<"clear">>',
                "oLanguage": { "sSearch": "" }
            });
        });
    </script>
    @endsection
