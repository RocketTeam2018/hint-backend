 @if ($errors->has("$slot"))
        <span class="has-danger">
            <strong>{{ $errors->first("$slot") }}</strong>
        </span>
    @endif
