<div id="left-sidebar" class="">
    <div class="bg"></div>
    <nav id="nav" class="sidebar-nav menu-scroll">

        <ul id="menu" class="side-nav metismenu">
            <!-- header section -->
            <li class="sidebar-header">Menu</li>

            <!-- single link -->
            <li><a href="{{route('index')}}" class="collapse "><i class="fa fa-home" aria-hidden="true"></i><span
                            class="drop-header">Home</span></a></li>

            <li><a href="{{route('admin.index')}}" class="collapse "><i class="fa fa-lock" aria-hidden="true"></i><span
                            class="drop-header">Admin </span></a></li>

            <li><a href="{{route('student.index')}}" class="collapse "><i class="fa fa-users"
                                                                          aria-hidden="true"></i><span
                            class="drop-header">student</span></a></li>
            <li><a href="{{route('tutor.index')}}" class="collapse "><i class="fa fa-user" aria-hidden="true"></i><span
                            class="drop-header">tutor </span></a></li>

            <li><a href="{{route('lab.index')}}" class="collapse "><i class="fa fa-location-arrow" aria-hidden="true"></i><span
                            class="drop-header">Lab </span></a></li>
            <li><a href="{{route('session.index')}}" class="collapse "><i class="fa fa-leanpub" aria-hidden="true"></i><span
                            class="drop-header">Session </span></a></li>
        </ul> <!-- /menu -->

    </nav> <!-- /sidebar-nav -->

</div>