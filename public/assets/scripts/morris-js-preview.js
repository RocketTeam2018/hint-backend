/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Area Chart
2.0 Bar Chart
3.0 Donut Chart
4.0 Line Chart with Data 
5.0 Resize Charts
------------------------------------------------------------*/

$(document).ready(function(){
'use strict'

/* 
Blue
area chart line color, bar chart color, donut chart color, line chart with data color 
*/
var color1 = "#304ffe";

/*
Darken Blue
area chart line darken color, bar chart darken color, donut chart darken color, line chart with data darken color 
*/
var darken_color1 = "#263fcb"

/*
Lighten Blue 
donut chart lighten color
*/
var lighten_color1 = "#5972fe"

/*----------------------------------------------------------
1.0 Area Chart
------------------------------------------------------------*/
function area_chart() {
    Morris.Area({
    element: 'morris_area_chart',
    data: [
        { y: '2006', a: 100, b: 90 },
        { y: '2007', a: 75,  b: 65 },
        { y: '2008', a: 50,  b: 40 },
        { y: '2009', a: 75,  b: 65 },
        { y: '2010', a: 50,  b: 40 },
        { y: '2011', a: 75,  b: 65 },
        { y: '2012', a: 100, b: 90 }
    ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        resize: true,
        redraw: true,
        lineColors:[darken_color1, color1],
        labels: ['Series A', 'Series B']
    });
}


area_chart();

/*----------------------------------------------------------
2.0 Bar Chart
------------------------------------------------------------*/
function bar_chart() {
    Morris.Bar({
        element: 'morris_bar_chart',
        data: [
            {x: '2016 Q1', y: 10, z: 5},
            {x: '2016 Q2', y: 7, z: 9},
            {x: '2016 Q3', y: 5, z: 4},
            {x: '2016 Q4', y: 7, z: 6}
        ],
        xkey: 'x',
        ykeys: ['y', 'z'],
        resize: true,
        redraw: true,
        barColors: [darken_color1, color1],
        labels: ['Y', 'Z']
    });
}

bar_chart();

/*----------------------------------------------------------
3.0 Donut Chart
------------------------------------------------------------*/
function donut_chart() {

Morris.Donut({
    element: 'morris_donut_chart',
    resize: true,
    redraw: true,
    data: [
        {value: 70, label: 'Chrome'},
        {value: 15, label: 'Firefox'},
        {value: 10, label: 'Explorer'},
    ],
        colors: [darken_color1, color1, lighten_color1],
        formatter: function (x) { return x + "%"}
    });
}

donut_chart();

/*----------------------------------------------------------
4.0 Line Chart with Data 
------------------------------------------------------------*/
function line_chart() {
    var timestamp_data = [
        {"period": 1349046000000, "licensed": 3407, "sorned": 660},
        {"period": 1313103600000, "licensed": 3351, "sorned": 700},
        {"period": 1299110400000, "licensed": 3269, "sorned": 728},
        {"period": 1281222000000, "licensed": 3246, "sorned": 891},
        {"period": 1273446000000, "licensed": 3257, "sorned": 777},
        {"period": 1268524800000, "licensed": 3248, "sorned": 707},
    ];
    Morris.Line({
        element: 'morris_line_chart',
        data: timestamp_data,
        resize: true,
        redraw: true,
        xkey: 'period',
        ykeys: ['licensed', 'sorned'],
        lineColors:[darken_color1, color1],
        labels: ['Licensed', 'SORN'],
        dateFormat: function (x) { return new Date(x).toDateString(); }
    });
}

line_chart();

/*----------------------------------------------------------
5.0 Resize Charts
------------------------------------------------------------*/
$(window).resize(function() {
    if (!window.recentResize) {
    window.area_chart.redraw();
    window.bar_chart.redraw();
    window.donut_chart.redraw();
    window.line_chart.redraw();
    window.recentResize = true;
    }
});


});//end
