/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Stats Style 1
	1.1 Stats Style 1.1
	1.2 Stats Style 1.2
	1.3 Stats Style 1.3
2.0 Location Map
------------------------------------------------------------*/

$(document).ready(function(){

'use strict';
var color1 = "#304ffe";
var light_color1 = "rgba(48, 79, 254, 0.5)";

var blue_color = "#304ffe";
var blue_rgba = "rgba(48, 79, 254, 0.5)";
var light_blue = "#4460fe"; 
var darken_blue_color = "#1c2f98";
var darken_blue_rgba = "rgba(33,55,177, 0.9)";

var green_color = "#00c853";
var green_rgba = "rgba(0, 200, 83, 0.5)";
var darken_green_color = "#007831";
var darken_green_rgba = "rgba(0,140,58, 0.9)";

var red_color = "#ff5252";     
var red_rgba = "rgba(255, 82, 82, 0.5)";
var darken_red_color = "#b23939";
var darken_red_rgba = "rgba(204,65,65, 0.9)";

var yellow_color = "#fdd835";     
var yellow_rgba = "rgba(253, 216, 53, 0.5)";

var white_color = "#ffffff";


/*----------------------------------------------------------
1.0 Stats Style 1
------------------------------------------------------------*/
$(function() {
var d1 = [[1,2],[2,7],[3,3],[4,9],[5,4],[6,6],[7,2],[8,4],[9,3]];
	
    $.plot("#stats_style_1", [ d1 ], {
       	series: {
            lines: {
			show: true,
			fill: true,
			fillColor: blue_rgba,
            },//lines
        },//series
        colors: [blue_color],
        grid: {
 		borderWidth: {top: 10, right: 0, bottom: 0, left: 0},
    	borderColor: {top: white_color},
		labelMargin: 0, 
		axisMargin: 0, 
		minBorderMargin: 0,
		hoverable: true,
		clickable: true 
	 	},//grid
		yaxis: {
		show: false
		},//yaxis
		xaxis: {
		show: false
		},//xaxis
        legend: false
    });//plot

    /*----------------------------------------------------------
	1.1 Stats Style 1.1
	------------------------------------------------------------*/
    $.plot("#stats_style_1_1", [ d1 ], {
       	series: {
            lines: {
			show: true,
			fill: true,
			fillColor: darken_blue_rgba,
            },//lines
        },//series
        colors: [darken_blue_color],
        grid: {
 		borderWidth: {top: 10, right: 0, bottom: 0, left: 0},
    	borderColor: {top: blue_color},
		labelMargin: 0, 
		axisMargin: 0, 
		minBorderMargin: 0,
		hoverable: true,
		clickable: true 
	 	},//grid
		yaxis: {
		show: false
		},//yaxis
		xaxis: {
		show: false
		},//xaxis
        legend: false
    });//plot    

    /*----------------------------------------------------------
	1.2 Stats Style 1.2
	------------------------------------------------------------*/
    $.plot("#stats_style_1_2", [ d1 ], {
       	series: {
            lines: {
			show: true,
			fill: true,
			fillColor: darken_green_rgba,
            },//lines
        },//series
        colors: [darken_green_color],
        grid: {
 		borderWidth: {top: 10, right: 0, bottom: 0, left: 0},
    	borderColor: {top: green_color},
		labelMargin: 0, 
		axisMargin: 0, 
		minBorderMargin: 0,
		hoverable: true,
		clickable: true 
	 	},//grid
		yaxis: {
		show: false
		},//yaxis
		xaxis: {
		show: false
		},//xaxis
        legend: false
    });//plot

    /*----------------------------------------------------------
	1.3 Stats Style 1.3
	------------------------------------------------------------*/
    $.plot("#stats_style_1_3", [ d1 ], {
       	series: {
            lines: {
			show: true,
			fill: true,
			fillColor: darken_red_rgba,
            },//lines
        },//series
        colors: [darken_red_color],
        grid: {
 		borderWidth: {top: 10, right: 0, bottom: 0, left: 0},
    	borderColor: {top: red_color},
		labelMargin: 0, 
		axisMargin: 0, 
		minBorderMargin: 0,
		hoverable: true,
		clickable: true 
	 	},//grid
		yaxis: {
		show: false
		},//yaxis
		xaxis: {
		show: false
		},//xaxis
        legend: false
    });//plot

});

/*----------------------------------------------------------
2.0 Location Map
------------------------------------------------------------*/
var world_map = $("#world-vmap");
world_map.vectorMap({
map: 'world_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

});//end