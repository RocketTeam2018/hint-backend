/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Basic Alert Example
2.0 Success Alert Example
3.0 Info Alert Example
4.0 Warning Alert Example
5.0 Danger Alert Example
6.0 Auto Close Alert Example
7.0 Custom Image Alert Example
8.0 Ip Alert Example
9.0 Message with Function Alert Example
10.0 Ajax Request Alert Example
11.0 Chaining Alert Example
12.0 Custom HTML Alert Example

------------------------------------------------------------*/
$(document).ready(function(){ 

'use strict';

/*----------------------------------------------------------
1.0 Basic Alert Example
------------------------------------------------------------*/
var basic_example = $("#basic_example");
basic_example.on("click", function(){
	swal('Any fool can use a computer')
});

/*----------------------------------------------------------
2.0 Success Alert Example
------------------------------------------------------------*/
var success_example = $("#success_example");
success_example.on("click", function(){
	swal(
	  'Good job!',
	  'You clicked the button!',
	  'success'
	)
});

/*----------------------------------------------------------
3.0 Info Alert Example
------------------------------------------------------------*/
var info_example = $("#info_example");
info_example.on("click", function(){
	swal(
	  'The Internet?',
	  'That thing is still around?',
	  'question'
	)
});

/*----------------------------------------------------------
4.0 Warning Alert Example
------------------------------------------------------------*/
var warning_example = $("#warning_example");
warning_example.on("click", function(){
	swal(
	  'The Internet?',
	  'That thing is still around?',
	  'warning'
	)
});

/*----------------------------------------------------------
5.0 Danger Alert Example
------------------------------------------------------------*/
var danger_example = $("#danger_example");
danger_example.on("click", function(){
	swal(
	  'Cancelled',
	  'Your imaginary file is safe :)',
	  'error'
	)
});

/*----------------------------------------------------------
6.0 Auto Close Alert Example
------------------------------------------------------------*/
var auto_close_example = $("#auto_close_example");
auto_close_example.on("click", function(){
	swal({
	  title: 'Auto close alert!',
	  text: 'I will close in 2 seconds.',
	  timer: 2000
	}).then(
	  function () {},
	  // handling the promise rejection
	  function (dismiss) {
	    if (dismiss === 'timer') {
	      console.log('I was closed by the timer')
	    }
	  }
	)
});

/*----------------------------------------------------------
7.0 Custom Image Alert Example
------------------------------------------------------------*/
var img_example = $("#img_example");
img_example.on("click", function(){
	swal({
	  title: 'Sweet!',
	  text: 'Modal with a custom image.',
	  imageUrl: 'https://unsplash.it/400/200',
	  imageWidth: 400,
	  imageHeight: 200,
	  animation: false,
	  showCloseButton: true
	})
});

/*----------------------------------------------------------
8.0 Ip Alert Example
------------------------------------------------------------*/
var ip_example = $("#ip_example");
ip_example.on("click", function(){
	swal.queue([{
	  title: 'Your public IP',
	  confirmButtonText: 'Show my public IP',
	  text:
	    'Your public IP will be received ' +
	    'via AJAX request',
	  showLoaderOnConfirm: true,
	  preConfirm: function () {
	    return new Promise(function (resolve) {
	      $.get('https://api.ipify.org?format=json')
	        .done(function (data) {
	          swal.insertQueueStep(data.ip)
	          resolve()
	        })
	    })
	  }
	}])
});

/*----------------------------------------------------------
9.0 Message with Function Alert Example
------------------------------------------------------------*/
var msg_function1_example = $("#msg_function1_example");
msg_function1_example.on("click", function(){
	swal({
	  title: 'Are you sure?',
	  text: "You won't be able to revert this!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!',
	  cancelButtonText: 'No, cancel!',
	  confirmButtonClass: 'btn btn-success',
	  cancelButtonClass: 'btn btn-danger',
	  buttonsStyling: false
	}).then(function () {
	  swal(
	    'Deleted!',
	    'Your file has been deleted.',
	    'success'
	  )
	}, function (dismiss) {
	  // dismiss can be 'cancel', 'overlay',
	  // 'close', and 'timer'
	  if (dismiss === 'cancel') {
	    swal(
	      'Cancelled',
	      'Your imaginary file is safe :)',
	      'error'
	    )
	  }
	})
});

/*----------------------------------------------------------
10.0 Ajax Request Alert Example
------------------------------------------------------------*/
var ajax_example = $("#ajax_example");
ajax_example.on("click", function(){
	swal({
	  title: 'Submit email to run ajax request',
	  input: 'email',
	  showCancelButton: true,
	  confirmButtonText: 'Submit',
	  showLoaderOnConfirm: true,
	  preConfirm: function (email) {
	    return new Promise(function (resolve, reject) {
	      setTimeout(function() {
	        if (email === 'taken@example.com') {
	          reject('This email is already taken.')
	        } else {
	          resolve()
	        }
	      }, 2000)
	    })
	  },
	  allowOutsideClick: false
	}).then(function (email) {
	  swal({
	    type: 'success',
	    title: 'Ajax request finished!',
	    html: 'Submitted email: ' + email
	  })
	})
});

/*----------------------------------------------------------
11.0 Chaining Alert Example
------------------------------------------------------------*/
var chaining_example = $("#chaining_example");
chaining_example.on("click", function(){
	swal.setDefaults({
	  input: 'text',
	  confirmButtonText: 'Next &rarr;',
	  showCancelButton: true,
	  animation: false,
	  progressSteps: ['1', '2', '3']
	})

	var steps = [
	  {
	    title: 'Question 1',
	    text: 'Chaining swal2 modals is easy'
	  },
	  'Question 2',
	  'Question 3'
	]

	swal.queue(steps).then(function (result) {
	  swal.resetDefaults()
	  swal({
	    title: 'All done!',
	    html:
	      'Your answers: <pre>' +
	        JSON.stringify(result) +
	      '</pre>',
	    confirmButtonText: 'Lovely!',
	    showCancelButton: false
	  })
	}, function () {
	  swal.resetDefaults()
	})
});

/*----------------------------------------------------------
12.0 Custom HTML Alert Example
------------------------------------------------------------*/
var html_example = $("#html_example");
html_example.on("click", function(){
	swal({
	  title: '<i>HTML</i> <u>example</u>',
	  type: 'info',
	  html:
	    'You can use <b>bold text</b>, ' +
	    '<a href="//github.com">links</a> ' +
	    'and other HTML tags',
	  showCloseButton: true,
	  showCancelButton: true,
	  confirmButtonText:
	    '<i class="fa fa-thumbs-up"></i> Great!',
	  cancelButtonText:
	    '<i class="fa fa-thumbs-down"></i>'
	})
});

}); //end