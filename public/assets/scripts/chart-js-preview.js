/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Line Chart
2.0 Bar Chart
3.0 Radar Chart
4.0 Doughnut Chart
5.0 Pie Chart
6.0 Polar Area Chart
------------------------------------------------------------*/

/*
Blue  
line chart label 1: background color  border color, point border color, point border hover color
radar chart: background color, border color, point color
doughnut chart / label 1 background color 
pie chart / label 1 background color
polar area chart: label 2 background color & hover background color
*/
var color1 = "#304ffe"; // Blue
var color1_rgba = "rgba(48, 79, 254,0.7)"; // bar chart label 1 / background color & border color
var radar_rgba_bg = "rgba(48, 79, 254,0.5)"  // radar rgba background color 
var radar_rgba_point = "rgba(48, 79, 254,1)"  // radar rgba color for points 

/*
Darken Blue 
line chart: point hover background colorr
doughnut chart / label 1 hover background color 
pie chart / label 1 hover background color
*/
var darken_color1 = "#263fcb"; // Darken Blue
var darken_color1_rgba = "rgba(48, 79, 254,0.9)"; // bar chart label 2/ background color & border color

/*
Lighten Blue 
line chart label 2: background color  border color, point border color, point border hover color
*/
var lighten_color1 = "#6e83fe"; // Lighten Blue

/*
Light Blue 
doughnut chart / label 2 background color
pie chart / label 2 background color
polar area chart / label 3 background color
*/
var light_color1 = "#4460fe"; // Light Blue

/*
Other Blue 
doughnut chart / label 2 background color hover
pie chart / label 2 background color hover
polar area chart / label 1 background color & background color hover
*/
var other_color = "#2b47e4"; // Other Blue

/* 
White 
Background color for points 
*/
var white_color = "#ffffff"; // White ;)

$(document).ready(function(){
'use strict';

/*----------------------------------------------------------
1.0 Line Chart
------------------------------------------------------------*/
var config = {
type: 'line',
	data: {
	labels: ["January", "February", "March", "April", "May", "June", "July"],
		datasets: [{
		label: "Label 1",
        data: [12, 30, 5, 22, 14, 40, 30],
        spanGaps: false,
        lineTension: 0.1,
		fill: false,
        backgroundColor: color1,
        borderColor: color1,
        pointBorderColor: color1,
        pointBackgroundColor: white_color,
        pointBorderWidth: 2,
        pointRadius: 4,
        pointHitRadius: 2,
        pointHoverBackgroundColor: darken_color1,
        pointHoverBorderColor: color1,
        pointHoverBorderWidth: 3,
        pointHoverRadius: 6,
		},
		{
		label: "Label 2",
        data: [6, 20, 7, 32, 8, 30, 24],
        spanGaps: false,
        lineTension: 0.1,
		fill: false,
        backgroundColor: lighten_color1,
        borderColor: lighten_color1,
        pointBorderColor: lighten_color1,
        pointBackgroundColor: white_color,
        pointBorderWidth: 2,
        pointRadius: 4,
        pointHitRadius: 2,
        pointHoverBackgroundColor: darken_color1,
        pointHoverBorderColor: lighten_color1,
        pointHoverBorderWidth: 3,
        pointHoverRadius: 6,
		}]
	},
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},
	}//options
};

var ctx = document.getElementById("line_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
2.0 Bar Chart
------------------------------------------------------------ */
var config = {
type: 'bar',
	data: {
	labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		datasets: [{
		label: "Label 1",
        data: [24, 30, 25 , 29, 32, 40, 30, 27, 35, 54, 39, 30],
        spanGaps: false,
        lineTension: 0.1,
		fill: false,
        backgroundColor: color1_rgba,
        borderColor: color1_rgba,
		},
		{
		label: "Label 2",
        data: [14, 18, 23 , 36, 29, 35, 49, 33, 40, 39, 59, 40],
        spanGaps: false,
        lineTension: 0.1,
		fill: false,
        backgroundColor: darken_color1_rgba,
        borderColor: darken_color1_rgba,
		}]
	},
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},
	}//options
};

var ctx = document.getElementById("bar_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
3.0 Radar Chart
------------------------------------------------------------ */
var config = {
type: 'radar',
	data: {
	labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
		datasets: [{
	        label: 'Google Chrome',
	        backgroundColor: radar_rgba_bg,
	        borderColor: color1,
	        pointBackgroundColor: radar_rgba_point,
	        pointBorderColor: color1,
	        pointHoverBackgroundColor: white_color,
	        pointHoverBorderColor: radar_rgba_point,
	        data : [65,30,80,71,76,60,50],
	        fill:true,
		    }]//datasets
	},//data
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},//legend
		scale: {
			ticks: {
			beginAtZero: true,
			display: false
			}//ticks
		}//scale
	}//options
};//config

var ctx = document.getElementById("radar_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
4.0 Doughnut Chart 
------------------------------------------------------------ */
var config = {
type: 'doughnut',
	data: {
	labels: ["Label 1", "Label 2",],
		datasets: [{
            data: [45,55],
            borderWidth:0,
	            backgroundColor: [
	            color1, //label1
	            light_color1  //label2
	            ],
	            hoverBackgroundColor: [
	            darken_color1, //label1
	            other_color  //label2
	            ]
		    }]//datasets
	},//data
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},//legend
		scale: {
			ticks: {
			beginAtZero: true,
			display: false
			}//ticks
		}//scale
	}//options
};//config

var ctx = document.getElementById("doughnut_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
5.0 Pie Chart
------------------------------------------------------------ */
var config = {
type: 'pie',
	data: {
	labels: ["Label 1", "Label 2",],
		datasets: [{
            data: [35,65],
            borderWidth:0,
	            backgroundColor: [
	            color1, //label1
	            light_color1  //label2
	            ],
	            hoverBackgroundColor: [
	            darken_color1, //label1
	            other_color  //label2
	            ]
		    }]//datasets
	},//data
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},//legend
		scale: {
			ticks: {
			beginAtZero: true,
			display: false
			}//ticks
		}//scale
	}//options
};//config

var ctx = document.getElementById("pie_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
6.0 Polar Area Chart
------------------------------------------------------------ */
var config = {
type: 'polarArea',
	data: {
	labels: ["Label 1", "Label 2", "Label 3"],
		datasets: [{
            data: [
    		11,
            8,
            5
            ],//data
            borderWidth:0,
	            backgroundColor: [
	            other_color, //label1
	            color1, //label2
	            light_color1, //label3
	            ],
	            hoverBackgroundColor: [
	            other_color, //label1
	            color1, //label2
	            light_color1, //label3
	            ]
		    }]//datasets
	},//data
	options: {
    maintainAspectRatio: false,
	responsive:true,
		legend: {
		display: true
		},//legend
		scale: {
			ticks: {
			beginAtZero: true,
			display: false
			}//ticks
		}//scale
	}//options
};//config

var ctx = document.getElementById("polar_chart");
new Chart(ctx, config);

}); //end