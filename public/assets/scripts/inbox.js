/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Toggle Inbox on Mobile Device
------------------------------------------------------------*/

$(document).ready(function(){
'use strict';

/*----------------------------------------------------------
1.0 Toggle Inbox on Mobile Device
------------------------------------------------------------*/
var inbox_toggle_btn = $("#toggle-mobile-sidebar");
var inbox_sidebar = $(".inbox-sidebar");
var inbox_toggle = $(".inbox-toggle");

inbox_toggle_btn.on('click', function(){
	inbox_sidebar.slideToggle(300);
	inbox_toggle.toggleClass("open");
});

});//end