/*----------------------------------------------------------
Template Name: Vega
Version: 1.0.0
Author: Zwolek
Author e-mail: zwoweb@gmail.com
Last change: 14/04/2017
------------------------------------------------------------*/

/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Preloader
2.0 Sidebar Menu Functions
3.0 Content Function
4.0 Notifications Scroll
5.0 Right Sidebar Function
6.0 Chat Functions
7.0 Message Dropdown Scroll
8.0 To Do Scroll
9.0 Search Functions
10.0 Mobile Header Functions
11.0 Mobile Sidebar Functions
12.0 Popover
13.0 Tooltip
14.0 Material Design Button Animation
15.0 Mobile Slide Btn
16.0 Theme Settings
17.0 Close Elements 
------------------------------------------------------------*/

$(document).ready(function(){

'use strict';

/*----------------------------------------------------------
1.0 Preloader
------------------------------------------------------------*/
/* $("#preloader").fakeLoader({
 timeToHide: 500, //Time in milliseconds for fakeLoader disappear
 zIndex:"99999",//Default zIndex
 spinner:"spinner7",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7',
 bgColor:"#ffffff" //Hex, RGB or RGBA colors
});*/

/*----------------------------------------------------------
2.0 Sidebar Menu Functions
------------------------------------------------------------*/
$('#menu').metisMenu();
var hamburger = $(".hamburger");
var mobile_header = $(".header-toggle .header-content");
var left_sidebar = $("#left-sidebar");
var footer = $(".footer");
var content = $("#content");

function resize() {
    if($(window).innerWidth() > 767) {
        left_sidebar.removeClass("sidebar-mobile"); 
        content.removeClass("full-width");
        mobile_header.removeClass("header-hidden");
        footer.removeClass("full-width");
        if($(hamburger).hasClass("bars")){
            hamburger.removeClass("bars").addClass("is-active");
        }
    } else {
        left_sidebar.addClass("sidebar-mobile"); 
        hamburger.removeClass("is-active").addClass("bars");
        content.addClass("full-width");
        mobile_header.addClass("header-hidden");
        footer.addClass("full-width");
    }
}

resize();

$(window).bind('resize',function(){
    resize();
});

hamburger.on("click", function(e) {
    e.preventDefault();
    hamburger.toggleClass("is-active");
});

/*----------------------------------------------------------
3.0 Content Function
------------------------------------------------------------*/
var side_nav = $("#nav");
var btn_collaspe = $("#menu-btn .hamburger-box");
var menu_btn = $("#menu-btn");
var hidden_sidebar = $("#hidden-btn");
var header_left = $(".header-left");

menu_btn.on('click', function(e){
  e.preventDefault();
    left_sidebar.toggleClass("collapse-sidebar");
    content.toggleClass("collapse-width");
    header_left.toggleClass("hidden");
    menu_btn.toggleClass("bg-brand");
    footer.toggleClass("collapse-footer");
    if($(side_nav).hasClass("opacity")) {
        side_nav.stop().animate({
            opacity: "0"
        }, 0);
        side_nav.stop().animate({
            opacity: "1"
        }, 2250);
        side_nav.removeClass("opacity");
    }
    else {
        side_nav.addClass("opacity");
        side_nav.stop().animate({
            opacity: "0"
        }, 0);
        side_nav.stop().animate({
            opacity: "1"
        }, 1000);
    }
});

hidden_sidebar.on('click', function(e){
    e.preventDefault();
    left_sidebar.toggleClass("close-sidebar");
    content.toggleClass("full-width");
    header_left.toggleClass("hidden");
    btn_collaspe.toggleClass("small");
    hidden_sidebar.toggleClass("bg-brand");
    footer.toggleClass("full-width");
    if($(side_nav).hasClass("opacity")) {
        side_nav.stop().animate({
            opacity: "0"
        }, 0);
        side_nav.stop().animate({
            opacity: "1"
        }, 2250);
        side_nav.removeClass("opacity");
    }
    else {
        side_nav.addClass("opacity");
        side_nav.stop().animate({
            opacity: "0"
        }, 0);
        side_nav.stop().animate({
            opacity: "1"
        }, 2250);
    }
});

/*----------------------------------------------------------
4.0 Notifications Scroll
------------------------------------------------------------*/
var notifications_scroll = $(".notifications-scroll");
notifications_scroll.slimScroll({
    position: 'right',
    height: '243px',
    wheelStep: 7,
    touchScrollStep: 200
});

var btn_mute = $("#btn-mute");
btn_mute.on('click', function(e){
    $("#icon-notification").toggleClass("fa-bell-slash");
    $(".status-pulse").toggleClass("hide");
});

/*----------------------------------------------------------
5.0 Right Sidebar Function
------------------------------------------------------------*/
var btn_slide = $('#btn-slide, #mobile-slide');
var right_sidebar = $("#right-sidebar");
btn_slide.on('click', function(e){
    right_sidebar.toggleClass("open-sidebar");
    return false;
});

var sidebar_tabs = $("#sidebar-tabs");
sidebar_tabs.on('click', function(e){
    e.preventDefault();
});

/*----------------------------------------------------------
6.0 Chat Functions
------------------------------------------------------------*/
var open_chat = $(".chat-item");
var single_message = $('.chat-msg');
open_chat.on('click', function(){
    single_message.toggleClass("open");
    return false;
    e.stopPropagation();
});

var close_chat = $("#close_chat");
close_chat.on('click', function(){
    single_message.removeClass("open");
});

var chat_scroll = $(".chat-scroll");
chat_scroll.slimScroll({
    position: 'right',
    height: '100%',
    wheelStep: 7,
    touchScrollStep: 200
});

var header_id = $("#header");
$(window).on('scroll', function(){
    if(window.scrollY > 55 && (!$(header_id).hasClass("header-fixed"))) {
        right_sidebar.addClass("scroll-sidebar");
    }
    else {
        right_sidebar.removeClass("scroll-sidebar");
    }
});

/*----------------------------------------------------------
7.0 Message Dropdown Scroll
------------------------------------------------------------*/
var msg_scroll = $(".msg-scroll");
msg_scroll.slimScroll({
    position: 'right',
    height: '100%',
    wheelStep: 7,
    touchScrollStep: 200
});

/*----------------------------------------------------------
8.0 To Do Scroll
------------------------------------------------------------*/
var todo_scroll = $(".todo-scroll");
todo_scroll.slimScroll({
    position: 'right',
    height: '100%',
    wheelStep: 7,
    touchScrollStep: 200
});

/*----------------------------------------------------------
9.0 Search Functions
------------------------------------------------------------*/
var search_box = $("#search");
var search_btn = $("#search-btn");
search_btn.on('click', function(){
    if((search_box).hasClass("open")) {
        search_box.removeClass("open");
    }//
    else {
        search_box.addClass("open");
    }
    return false;
    e.stopPropagation();
});

/*----------------------------------------------------------
10.0 Mobile Header Functions
------------------------------------------------------------*/
var mobile_btn = $("#mobile-btn");
mobile_btn.on('click', function(){
    mobile_header.slideToggle(300);
});

/*----------------------------------------------------------
11.0 Mobile Sidebar Functions
------------------------------------------------------------*/
var mobile_sidebar = $("#mobile-menu");
var page = $("#page");
mobile_sidebar.on('click', function(){
    left_sidebar.toggleClass("sidebar-mobile");
    content.toggleClass("active-bg"); 
});

/*----------------------------------------------------------
12.0 Popover
------------------------------------------------------------*/
$("[data-toggle=popover]").popover();

/*----------------------------------------------------------
13.0 Tooltip
------------------------------------------------------------*/
$('[data-toggle=tooltip]').tooltip();

/*----------------------------------------------------------
14.0 Material Design Button Animation
------------------------------------------------------------*/
var btn_ripple = $(".md-ripple");
btn_ripple.on("click", function(e){
    var rippler = $(this);
    if(rippler.find(".ripple").length == 0) {
        rippler.append("<span class='ripple'></span>");
    }
    var ripple = rippler.find(".ripple");
    ripple.removeClass("animate");
    if(!ripple.height() && !ripple.width()) {
        var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
        ripple.css({height: d, width: d});
    }
    var x = e.pageX - rippler.offset().left - ripple.width()/2;
    var y = e.pageY - rippler.offset().top - ripple.height()/2;
    ripple.css({
        top: y+'px',
        left: x+'px'
    }).addClass("animate");
});

/*----------------------------------------------------------
15.0 Mobile Slide Btn
------------------------------------------------------------*/
var mobile_slide_btn = $("#mobile-slide");
mobile_slide_btn.on('click', function(e){
    mobile_header.slideToggle(300);
    e.preventDefault();
});

/*----------------------------------------------------------
16.0 Theme Settings
------------------------------------------------------------*/
var btn_settings = $("#button-settings");
var theme_settings = $("#theme-settings");

btn_settings.on('click', function(e) {
    btn_settings.toggleClass("active");
    theme_settings.toggleClass("active");
e.preventDefault();
});

/*----------------------------------------------------------
17.0 Close Elements 
------------------------------------------------------------*/
content.on('click', function() { 
    right_sidebar.removeClass("open-sidebar");
    search_box.removeClass("open");
    single_message.removeClass("open");
    if($(window).innerWidth() < 768) {
        mobile_header.slideToggle(300);    
    }//if
    if($(btn_settings).hasClass("active") && $(theme_settings).hasClass("active")){
        btn_settings.removeClass("active");      
        theme_settings.removeClass("active");
    }
});

var header = $(".header-content .left, .header-content .nav");
var close_search = left_sidebar.add(header).add(btn_slide);

close_search.on('click', function() { 
    search_box.removeClass("open");
});


});//end document