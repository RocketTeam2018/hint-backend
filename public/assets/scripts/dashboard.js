/*----------------------------------------------------------
Template Name: Vega
Version: 1.0.0
Author: Zwolek
Author e-mail: zwoweb@gmail.com
Last change: 10/04/2017
------------------------------------------------------------*/

/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 Sale Chart
2.0 Earnings Chart
3.0 Visits Chart
4.0 Datatables
5.0 Location Map
------------------------------------------------------------*/

$(document).ready(function(){

'use strict';
/*
Blue  
Sale Chart, Earnings Chart, Visits Chart, Location Map
*/
var color1 = "#304ffe";
var color1_rgba = "rgba(48, 79, 254,0.7)";

/*
Darken Blue
Earnings Chart, Visits Chart,
*/
var darken_color1 = "#263fcb";
var darken_color1_rgba = "rgba(48, 79, 254,0.9)";


/*
Light Blue
Sale Chart, Location Map
*/
var light_color1 = "#4460fe"; 

/* 
White 
Sale Chart
*/
var white_color = "#ffffff";
var white_rgba = "rgba(255,255,255, 0)"; 

/*
Gray
Sale Chart, 
*/
var gray = "#f2f2f2";

/*----------------------------------------------------------
1.0 Sale Chart
------------------------------------------------------------*/
var config = {
type: 'line',
    data: {
    labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
        datasets: [
            {
            label: "Label 1",
            data: [14, 4, 4, 10, 10, 4, 4, 10, 10, 4, 4, 14],
            lineTension: 0.4,
            backgroundColor: light_color1,
            borderColor: light_color1,
            pointBorderColor: light_color1,
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 10,
            },                  
            {
            label: "Label 2",
            data: [14, 7, 7, 13, 13, 7, 7, 13, 13, 7, 7, 13],
            lineTension: 0.4,
            backgroundColor: color1,
            borderColor: color1,
            pointBorderColor: color1,
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 10,
            },        
            {
            label: "Label 3",
            data: [14, 12, 15, 12, 12, 15, 15, 12, 12, 15, 12, 14],
            lineTension: 0.4,
            backgroundColor: gray,
            borderColor: gray,
            pointBorderColor: gray,
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 10,
            },                            
            {
            label: "Label 4",
            data: [30],
            lineTension: 0.4,
            backgroundColor: white_rgba,
            borderColor: white_rgba,
            pointBorderColor: white_rgba,
            pointBorderWidth: 0,
            pointRadius: 0,
            pointHitRadius: 10,
            },                 
        ]
    },
    options: {
    maintainAspectRatio: false,
    responsive:true,
        legend: {
        display: false
        },
        scales: {
        xAxes: [{display: false}],
        yAxes: [{
            ticks: {
            display: false
            },
            gridLines: {
            drawBorder: false, 
            tickMarkLength: 0
            }
        }]
        },
        tooltips: {
        enabled: false      
        }
    }//options
};

var ctx = document.getElementById("sale_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
2.0 Earnings Chart
------------------------------------------------------------*/
var config = {
type: 'bar',
    data: {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
        label: "Monthly Earnings",
        data: [24, 30, 25 , 29, 32, 40, 30],
        spanGaps: false,
        lineTension: 0.5,
        fill: false,
        backgroundColor: color1_rgba,
        borderColor: color1_rgba,
        },
        {
        label: "Target",
        data: [14, 18, 23 , 36, 29, 35, 49],
        spanGaps: false,
        lineTension: 0.5,
        fill: false,
        backgroundColor: darken_color1_rgba,
        borderColor: darken_color1_rgba,
        }]
    },
    options: {
    maintainAspectRatio: false,
    responsive:true,
        legend: {
        display: false
        },
        scales: {
            xAxes: [{
            display: false
            }],
            yAxes: [{
                gridLines: {
                tickMarkLength: 0
                }
            }]
        }
    }//options
};

var ctx = document.getElementById("earnings_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
3.0 Visits Chart
------------------------------------------------------------*/
var config = {
type: 'line',
    data: {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
        label: "Unique Visitors",
        data: [10, 30, 15, 22, 14, 38, 30],
        spanGaps: false,
        lineTension: 0.1,
        fill: false,
        borderColor: color1,
        pointBorderWidth: 0,
        pointRadius: 0,
        pointHitRadius: 10,
        pointHoverBackgroundColor: darken_color1,
        pointHoverBorderColor: color1,
        pointHoverBorderWidth: 2,
        pointHoverRadius: 4,
        }]
    },
    options: {
    maintainAspectRatio: false,
    responsive: true,
        legend: {
        display: false
        },
        scales: {
            xAxes: [{
            display: false
            }],
            yAxes: [{
                gridLines: {
                tickMarkLength: 0
                }
            }]
        }
    }//options
};

var ctx = document.getElementById("visitors_chart");
new Chart(ctx, config);

/*----------------------------------------------------------
4.0 Datatables
------------------------------------------------------------*/
var table = $('#dashboard-table');
table.DataTable({
    "paging":   false,
    "ordering": true,
    "dom": '<<"" <"dataTable_top left col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-0"i<"clear">> <"dataTable_top right col-xs col-sm-12 col-md-6 col-lg-6 col-xl-6 pr-0"f<"clear">> >rt<"bottom"p<"clear">>',
    "oLanguage": { "sSearch": "" },
    columnDefs: [ {
        targets: [ 0 ],
        orderData: [ 0, 1 ]
    }, {
        targets: [ 1 ],
        orderData: [ 1, 0 ]
    }, {
        targets: [ 4 ],
        orderData: [ 4, 0 ]
    } ]
});

/*----------------------------------------------------------
5.0 Location Map
------------------------------------------------------------*/
var world_map = $("#world-vmap");
world_map.vectorMap({
map: 'world_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

}); //end