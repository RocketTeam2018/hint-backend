/*----------------------------------------------------------
TABLE OF CONTENTS:
------------------------------------------------------------
1.0 World Vector Map
2.0 Europe Vector Map
3.0 Usa Vector Map
4.0 Russia Vector Map
------------------------------------------------------------*/

$(document).ready(function(){ 

'use strict';

// Blue (Map Color)
var color1 = "#304ffe";

// Light Blue (Select Map Color) 
var light_color1 = "#2137b1";

/*----------------------------------------------------------
1.0 World Vector Map
------------------------------------------------------------*/
var world_map = $("#world-vmap");
world_map.vectorMap({
map: 'world_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

/*----------------------------------------------------------
2.0 Europe Vector Map
------------------------------------------------------------*/
var europe_map = $("#europe-vmap");
europe_map.vectorMap({
map: 'europe_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

/*----------------------------------------------------------
3.0 Usa Vector Map
------------------------------------------------------------*/
var usa_map = $("#usa-vmap");
usa_map.vectorMap({
map: 'usa_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

/*----------------------------------------------------------
4.0 Russia Vector Map
------------------------------------------------------------*/
var russia_map = $("#russia-vmap");
russia_map.vectorMap({
map: 'russia_en',
backgroundColor: null,
color: color1,
hoverOpacity: 0.7,
selectedColor: light_color1,
enableZoom: true,
showTooltip: true,
scaleColors: [color1, color1],
normalizeFunction: 'polynomial'
});

});//end