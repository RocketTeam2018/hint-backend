<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /*'id', 'courseName', 'labID', 'token', 'TutorsID', 'time', 'notes'*/
    //
    protected $fillable=[ 'courseName', 'labID', 'token', 'TutorID', 'time', 'notes'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function lab(){
        return $this->belongsTo('App\Location','labID','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function doctor(){
        return $this->belongsTo('App\User','TutorID','uuid');
    }
}
