<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attend extends Model
{
    protected $fillable=['StudentID','SessionID'];

    /**
     * @return $this
     */
    function session(){
        return $this->belongsTo('App\Session','SessionID','id')->with('doctor');
    }

    /**
     * @return $this
     */
    function student(){
        return $this->belongsTo('App\User','StudentID','uuid')->orderBy('name','asc');
    }

}
