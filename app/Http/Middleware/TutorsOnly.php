<?php

namespace App\Http\Middleware;

use Closure;

class TutorsOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user('api')!=null){
            if ($request->user()->type == 0 && $request->user()) {
                return response()->json(['message'=>'UnAuthorized']);
            }
        }

        if ($request->user('web')!=null){
            if ($request->user()->type == 0 && $request->user()) {
                return redirect('/notTutor');
            }
        }
        return $next($request);
    }
}
