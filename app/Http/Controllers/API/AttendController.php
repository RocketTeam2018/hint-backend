<?php

namespace App\Http\Controllers\API;

use App\Attend;
use App\Http\Controllers\Controller;
use App\Session;
use Illuminate\Http\Request;

class   AttendController extends Controller
{
    public function __construct()
    {
        $this->middleware('TutorsOnly')->only(['show','attendTutor']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $session = $request['SessionID'];
        $studentID = auth()->user()->uuid;
        if(Attend::where('StudentID',$request['studnet'])->where('SessionID',$session)->exists() or Attend::where('StudentID',$studentID)->where('SessionID',$session)->exists()){
            return response()->json(['message'=>'user already attended']);
        }
        if ($request->has('student')) {
            $studentID = $request['student'];
            Attend::create(['SessionID' => $session, 'StudentID' => $studentID]);
            return response()->json(['message' => 'success']);
        }
        if ($request['token']==Session::find($session)->token){
        Attend::create(['SessionID' => $session, 'StudentID' => $studentID]);
        return response()->json(['message' => 'success']);
        }
        return response()->json(['message'=>'please try again']);
    }

    public function showById(Request $request)
    {
        $attends = Attend::where('SessionID', $request['id'])->with('student')->get();

        return response()->json(['session' => Session::find($request['id']), 'attends' => $attends]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* $attends=Attend::where('SessionID',$id)->with('student')->get();

         return response()->json(['session'=>Session::find($id),'attends'=>$attends]);*/

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function attendTutor(Request $request)
    {
        $tutor = auth()->user()->uuid;
        if (Session::find($request['session'])->TutorID == $tutor) {
            Attend::where('SessionID', $request['session'])->where('StudentID', $request['student'])->first()->delete();
            return response()->json(['message' => 'deleted successfully !']);
        }
        return response()->json(['message' => 'unAuthorized !']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function isAttend(Request $request)
    {
        $isattend = Attend::where('SessionID', $request['session'])->where('StudentID', $request['student'])->exists();
        return response()->json(['isAttend' => $isattend]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
