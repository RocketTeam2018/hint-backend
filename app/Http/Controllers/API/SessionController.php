<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Session;
use Illuminate\Http\Request;
use Validator;

class   SessionController extends Controller
{
    function __construct()
    {
        $this->middleware('TutorsOnly')->only(['store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['sessions'=>Session::with('doctor','lab')->get()],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = ['courseName' => 'required', 'labID' => 'required','time'=>'date','notes'=>'string'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response(['status' => false, 'message' => $validator->messages()]);

        } else {
            $session=Session::create($request->all() + ['TutorID'=>auth()->user()->uuid,'token'=>str_random(50)]);
            return response()->json(['session'=>$session]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['session'=>Session::with('doctor','lab')->where('id',$id)->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SessionByDate(Request $request){
        return response()->json(['sessions'=>Session::with('doctor','lab')->whereDate('time',$request['date'])->orderBy('time','asc')->get()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function SessionTutorByDate(Request $request){
        return response()->json(['sessions'=>Session::with('doctor','lab')->whereDate('time',$request['date'])->where('TutorID',auth()->user()->uuid)->get()]);
    }
}
