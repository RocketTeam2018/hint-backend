<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Photo;
use Illuminate\Http\Request;
use LaravelImage\ImageUploadService;

class photoController extends Controller
{
    protected $file;

    /**
     * @param ImageUploadService $file
     */
    public function __construct(ImageUploadService $file)
    {

        //set properties for file upload
        $this->file = $file;
        $this->file->setUploadField('images[]'); //default is image
        $this->file->setUploadFolder('tutor');
    }
    public function upload(Request $request)
    {   
        foreach ($request['images'] as $image) {
            $file = $image;
            $filename = $file->getClientOriginalName();
            $file->move("uploads/training/".$request['uuid'], $filename);
            Photo::create(['uuid'=>$request['uuid'],'image'=>'/uploads/training/'.$request['uuid'].'/'. $filename]);
        }
        return response()->json(['message'=>'Upload successful '.count($request['images']).' images']);
    }
}
