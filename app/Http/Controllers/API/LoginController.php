<?php

namespace App\Http\Controllers\API;
use Token;
use App\Student;
use App\Tutor;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    function Login(Request $request){
        $rules = ['email' => 'required|email', 'password' => 'required',];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response(['status' => false, 'message' => $validator->messages()]);

        } else {
           // dd(auth()->attempt(['email' => $request['email'], 'password' => $request['password']]));
            if (auth()->attempt(['email' => $request['email'], 'password' => $request['password']])) {
                $user = auth()->user();
                $user->api_token = Token::Unique('users','api_token', 100,true );
                $user->save();
                return response(['token' => $user->api_token,'user'=>User::where('uuid',$user->uuid)->first()], 200);
            } else {
                return response()->json(['status'=>false,'message' => 'Wrong Cardinals'], 404);
            }
        }

    }
}
