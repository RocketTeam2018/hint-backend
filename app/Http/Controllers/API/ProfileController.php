<?php

namespace App\Http\Controllers\API;

use App\Attend;
use App\Http\Controllers\Controller;
use App\Session;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('TutorsOnly')->only(['allSessions']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function byDate(Request $request){
        return response()->json(['attends' => Attend::whereDate('created_at',$request['date'])->where('StudentID', auth()->user()->uuid)->with('session')->orderBy('created_at', 'desc')->get()]);
    }

    /**
     * return all student attends
     * @return \Illuminate\Http\JsonResponse
     */
    public function allAttends()
    {
        return response()->json(['attends' => Attend::where('StudentID', auth()->user()->uuid)->with('session')->orderBy('created_at', 'desc')->get()]);
    }


    /**
     * كل المحاضرات اللي حضراها الطالب ده للدكتور ده
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function allTutorAttends(Request $request)
    {
        $student = $request['student'];
        $tutor = $request['tutor'];

        $attends = Attend::whereHas('session', function ($query) use ($tutor) {
            $query->where('TutorID', $tutor);
        })->where('StudentID', $student)->with('session')->get();
        return response()->json(['attends' => $attends]);
    }

    /**
     *  return all sessions with lab data
     * @return \Illuminate\Http\JsonResponse
     */
    public function allSessions()
    {
        return response()->json(['sessions' => Session::where('TutorID', auth()->user()->uuid)->with('lab')->get()]);
    }

    public function updatePassword(Request $request)
    {
        $rules = ['old_password' => 'required', 'password' => 'required|min:6'];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response(['status' => false, 'message' => $validator->messages()]);

        }
        $user = auth()->user();

        // dd(hash::check($request['old_password'],$user->password));
        if (hash::check($request['old_password'], $user->password)) {
            $user->password = bcrypt($request['password']);
            $user->save();
            return response()->json(['message' => 'success !']);
        }
        return response()->json(['message' => "something wrong"], 406);

    }

    public function SetMac(Request $request)
    {
        $user=User::where('uuid',$request['uuid'])->first();
        if ($user->MAC==null){
        $user->MAC=$request['MAC'];
        $user->save();
        return response()->json(['message'=>'success !']);
        }
        else{
            return response()->json(['message'=>'MAC ERROR']);
        }
    }


}
