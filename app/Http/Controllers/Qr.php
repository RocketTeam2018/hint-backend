<?php

namespace App\Http\Controllers;

use App\Session;
use Token;
use Illuminate\Http\Request;

class Qr extends Controller
{
    public function __construct()
    {
        $this->middleware('TutorsOnly');
    }

    public function index()
    {
        $id = auth()->user()->uuid;
        return view('tutor.welcome')->with('sessions',Session::where('tutorID', $id)->with('doctor')->orderByDesc('time')->get());

    }

    public function Qr($id)
    {
        return view('tutor.QR')->with('session', Session::where('id', $id)->orderByDesc('time')->first());
    }

    public function refresh($id)
    {
        $token=Token::Unique('users', 'api_token', 100, true);
        $session = Session::where('id', $id)->orderByDesc('time')->first()->update([
            'token'=>$token
        ]);

        return response()->json(['token' => $token], 200);
    }
}


