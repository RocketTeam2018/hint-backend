<?php

namespace App\Http\Controllers\AdminPanel;

use Alert;
use App\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\addAdmin;
use App\Http\Requests\updatePassword;
use Illuminate\Http\Request;

class adminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminPanel.admin.index')->with('admins',Admin::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(addAdmin $request)
    {
        Admin::create($request->except('password') + ['password'=>bcrypt($request['password'])]);
        alert()->success('Success Message')->autoclose(1000);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('adminPanel.admin.edit')->with('admin',Admin::where('id',$id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user=Admin::where('id',$id)->first();
        $user->update($request->only('name'));
        alert()->success('Success Message')->autoclose(1000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updatePassword(updatePassword $request,$id)
    {
        $user = Admin::where('id', $id)->first();
        $user->update(['password' => bcrypt($request['password'])]);
        Alert::success('Password Edified Successfully !')->persistent('Close');
        return back();
    }

}
