<?php

namespace App\Http\Controllers\AdminPanel;

use App\Http\Controllers\Controller;
use App\Http\Requests\labAdd;
use App\Location;
use Illuminate\Http\Request;
use App\Session;

class labController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminPanel.lab.index')->with('labs', Location::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.lab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(labAdd $request)
    {
        Location::create($request->all());
        alert()->success('Success Message', 'Optional Title')->autoclose(1000);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('adminPanel.lab.show')->with('lab',Location::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('adminPanel.lab.edit')->with('lab',Location::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(labAdd $request, $id)
    {
        Location::find($id)->update($request->all());
        alert()->success('Success Message', 'Optional Title')->autoclose(1000);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function session($id){
        return view('adminPanel.session.index')->with('sessions',Session::with('lab','doctor')->where('labID',$id)->get());
    }
}
