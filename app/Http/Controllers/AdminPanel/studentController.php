<?php

namespace App\Http\Controllers\AdminPanel;

use App\Attend;
use App\Http\Controllers\Controller;
use App\Http\Requests\addUser;
use App\Http\Requests\updatePassword;
use App\Http\Requests\updateUser;
use App\User;
use Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use LaravelImage\ImageUploadService;
use Alert;
class studentController extends Controller
{
    protected $file;

    /**
     * @param ImageUploadService $file
     */
    public function __construct(ImageUploadService $file)
    {

        //set properties for file upload
        $this->file = $file;
        $this->file->setUploadField('photo'); //default is image
        $this->file->setUploadFolder('students');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('adminPanel.student.index')->with('students',User::where('type',0)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('adminPanel.student.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(addUser $request)
    {
        //dd($request->all());
       // dd(Token::Unique('users','api_token',100,true));
        $image='';
        if (Input::hasFile('photo') && $this->file->upload()) {
            $image = $this->file->getUploadedFileInfo()['upload_dir']. $this->file->getUploadedFileInfo()['photo'];
        }
        User::create($request->except('photo','password') + ['image'=>$image,
                'api_token'=>Token::Unique('users','api_token', 100,true ),'password'=>bcrypt($request['password']),
                'type'=>0]);

        alert()->success('Success Message', 'Optional Title')->autoclose(1000);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('adminPanel.student.show')->with('student',User::where('uuid',$id)->first());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      //  dd(User::where('uuid',23423)->first());
        return view('adminPanel.student.edit')->with('student',User::where('uuid',$id)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(updateUser $request, $id)
    {
        $user=User::where('uuid',$id)->first();
        $image=$user->image;

        if (Input::hasFile('photo') && $this->file->upload()) {
            $image = $this->file->getUploadedFileInfo()['upload_dir']. $this->file->getUploadedFileInfo()['photo'];
        }
        $user->update($request->except('photo') + ['image'=>$image,
                'api_token'=>Token::Unique('users','api_token', 100,true ),
                'type'=>0]);

        alert()->success('Success Message', 'Optional Title')->autoclose(1000);
        return back();
    }

    /**
     * @param UpdatePassword $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(updatePassword $request,$id)
    {
        $user = User::where('uuid', $id)->first();
        $user->update(['password' => bcrypt($request['password'])]);
        Alert::success('Password Edified Successfully !')->persistent('Close');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('uuid',$id)->delete();
        Alert::success('User deleted Successfully !')->autoclose(1000);
        return back();
    }

    public function attends($id)
    {
       // dd(Attend::where('studentID',$id)->with('session')->get());
        return view('adminPanel.attends.index')->with('attends',Attend::where('studentID',$id)->with('session')->get());
    }

    public function resethMAC($id)
    {
        User::where('uuid',$id)->first()->update(['MAC'=>null]);
        Alert::success('MAC Reset Successfully !')->autoclose(1000);
        return back();
    }
}
