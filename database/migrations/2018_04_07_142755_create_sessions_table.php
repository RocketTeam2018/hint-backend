<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('courseName');
            $table->unsignedInteger('labID');
            $table->string('token');
            $table->foreign('labID')
                ->references('id')->on('locations')
                ->onDelete('cascade');
            $table->string('TutorID');
            $table->foreign('TutorID')
                ->references('uuid')->on('users')
                ->onDelete('cascade');
            $table->dateTime('time');
            $table->text('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
